using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeHealth : MonoBehaviour
{
    public SlimeBehavior slimeBehavior;

    public void Hit( int damage, Vector3 hitPos, float attackRadius, int kbBase = 300, int kbFactor = 500) {
        slimeBehavior.Hit(damage, hitPos, attackRadius, kbBase, kbFactor);
    }
}
