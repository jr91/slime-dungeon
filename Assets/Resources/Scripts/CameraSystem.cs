﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSystem : MonoBehaviour {

    public Transform player;
    public Transform currentCell;

    public float smoothSpeed = 0.125f;
    public Vector3 offset;

    void Start()
    {
        //Snap camera to player location immediately on start
        transform.position = player.position + offset;

        //This exists to stop the script from throwing an error, since the currentCell variable is null for a fraction of a second before the player script locates the first cell
        currentCell = player;
    }

    void FixedUpdate()
    {
        Vector3 desiredPostion = new Vector3((currentCell.position.x + player.position.x) / 2, (currentCell.position.y + player.position.y) / 2, transform.position.z);
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPostion, smoothSpeed);

        transform.position = smoothedPosition;
    }
}
