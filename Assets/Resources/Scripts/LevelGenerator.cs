﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.IO;

public class LevelGenerator : MonoBehaviour {

    //Level generator variables
    public Texture2D[] maps;
    public Tilemap levelTileMap;

    public Tilemap backTileMap;
    public RuleTile backtile;

    public ColorToTile[] tiles;

    public ColorToPrefab[] prefabs;

    public Color mobColor;
    public GameObject[] mobPrefabs;

    // public ColorToPrefab[] mobPrefabs;
    public int minMobsPerCell = 2;
    public int maxMobsPerCell = 5;

    //Player spawnpoint variables
    public Color playerSpawnpointColor;
    public Transform player;
    public Texture2D playerSpawn;

    public Texture2D endZone;
    public GameObject cellObject;

    private Texture2D fullMap;

    private int[,,] offsets = new int[,,] 
    {
        { {0,48},{16,48},{32,48},{48,48} },
        { {0,32},{16,32},{32,32},{48,32} },
        { {0,16},{16,16},{32,16},{48,16} },
        { {0,0},{16,0},{32,0},{48,0} }
    };

	void Start () {

        //Generate new random map
        fullMap = GenerateMap();

        //Generate map if proper files are present, else log an error.
        if (fullMap && levelTileMap)
        {
            GenerateLevel();
        }
        else
        {
            Debug.LogError("Cannot generate map: map files not found.");
        }
        
	}

    void GenerateLevel()
    {
        for(int x = 0; x < fullMap.width; x++)
        {
            for (int y = 0; y < fullMap.height; y++)
            {
                GenerateTile(x, y);
            }
        }
    }

    void GenerateTile(int x, int y)
    {
        Color pixelColor = fullMap.GetPixel(x, y);

        if (!(pixelColor.r == 0 && pixelColor.g == 0 && pixelColor.b == 0 && pixelColor.a != 0)) 
        {
            //Draw floor tiles under everything that isn't a wall tile
            backTileMap.SetTile(new Vector3Int(-(-x + fullMap.width / 2), -(-y + fullMap.height / 2), 0), backtile);
        }
        
        if (pixelColor.a == 0)
        {
            //Ignore transparent pixels
            return;
        }

        if (playerSpawnpointColor.Equals(pixelColor))
        {
            //move player to spawnpoint if pixel is the designated color
            player.transform.position = new Vector2((x + 0.5f) - fullMap.width / 2, (y + 0.5f) - fullMap.height / 2);
            return;
        }

        foreach (ColorToTile tile in tiles)
        {
            if (tile.color.Equals(pixelColor) && pixelColor != playerSpawnpointColor)
            {
                levelTileMap.SetTile(new Vector3Int(-(-x + fullMap.width / 2), -(-y + fullMap.height / 2), 0), tile.tile);
                return;
            }
        }

        foreach (ColorToPrefab prefab in prefabs)
        {
            if (prefab.color.Equals(pixelColor))
            {
                Vector2 position = new Vector2((x + 0.5f) - fullMap.width / 2, (y + 0.5f) - fullMap.height / 2);
                Instantiate(prefab.prefab, position, Quaternion.identity, transform);
            }
        }

        if(mobColor.Equals(pixelColor))
        {
            Vector2 position = new Vector2((x + 0.5f) - fullMap.width / 2, (y + 0.5f) - fullMap.height / 2);
            int r = Random.Range(0, mobPrefabs.Length);
            Instantiate(mobPrefabs[r], position, Quaternion.identity, transform);
        }

    }

    void DrawCell(Texture2D map, Texture2D result, int offsetX, int offsetY, bool populate = false){

        Color wallColor = new Color (0,0,0);
        wallColor = tiles[0].color;

        //Treasure chest
        int spawnChest = Random.Range(0, 12);
        int[] chest = new int[2] {Random.Range(6,9), Random.Range(6,9)} ;

        //Potion
        int spawnPotion = Random.Range(0, 20);
        int[] potion = new int[2] {Random.Range(6,9), Random.Range(6,9)} ;

        //Mobs
        int spawnMobs = Random.Range(minMobsPerCell, maxMobsPerCell);
        List<int[]> mobInstances = new List<int[]> ();
        for (int i = spawnMobs; i > 0; i--){
            mobInstances.Add(new int[2] {Random.Range(1,14), Random.Range(1,14)});
        }

        //Instantiate cell marker object in game
        Vector2 position = new Vector2((offsetX + 8) - result.width / 2, (offsetY + 8) - result.height / 2);
        Instantiate(cellObject, position, Quaternion.identity, transform);

        //Draw cell on map
        for(int x = 0; x < map.width; x++)
        {
            for (int y = 0; y < map.height; y++)
            {
                if( !((x + offsetX != 0 && x + offsetX < result.width - 1) && (y + offsetY != 0 && y + offsetY < result.height - 1)) )
                {
                    //if pixel is on edge of map, fill it in with wall
                    result.SetPixel(x + offsetX, y + offsetY, wallColor);
                }
                else if(populate == true) {
                    if(map.GetPixel(x,y).a == 0 )
                    {
                        if(spawnChest == 1 && x == chest[0] && y == chest[1]) 
                        {
                            result.SetPixel(x + offsetX, y + offsetY, prefabs[1].color);
                        }
                        else if(spawnPotion == 1 && x == potion[0] && y == potion[1])
                        {
                            result.SetPixel(x + offsetX, y + offsetY, prefabs[2].color);
                        }
                        else if(spawnMobs > 0) 
                        {
                            if(mobInstances.Exists(i => i[0] == x && i[1] == y))
                            {

                                result.SetPixel(x + offsetX, y + offsetY, mobColor);
                                spawnMobs--;
                            }
                        }
                        else {
                            result.SetPixel(x + offsetX, y + offsetY, map.GetPixel(x,y));
                        }

                    }
                    else {
                        result.SetPixel(x + offsetX, y + offsetY, map.GetPixel(x,y));
                    }
                }
                else {
                    result.SetPixel(x + offsetX, y + offsetY, map.GetPixel(x,y));
                }
            }
        }

        result.Apply();
    }

    void DrawWallCell(Texture2D result, int offsetX, int offsetY){

        Color wallColor = new Color (0,0,0);

        for(int x = 0; x < 16; x++)
        {
            for (int y = 0; y < 16; y++)
            {
                result.SetPixel(x + offsetX, y + offsetY, wallColor);
            }
        }

        result.Apply();
    }

    protected Texture2D GenerateMap()
    {
        //Generate layout matrix
        int[,] layout = GenerateLayout();

        //Generate new blank map
        Texture2D result = new Texture2D(64,64);

        //Generate map cells
        for (int i = 0; i <= layout.GetUpperBound(0); i++) 
        {
            for (int j = 0; j <= layout.GetUpperBound(1); j++) 
            {
                if (layout[i,j] == 2) {
                    DrawCell(playerSpawn, result, offsets[i,j,0], offsets[i,j,1]);
                } 
                else if ( layout[i,j] == 1) {
                    DrawWallCell(result, offsets[i,j,0], offsets[i,j,1]);
                }
                else if ( layout[i,j] == 0) {
                    DrawCell(RandomMap(maps), result, offsets[i,j,0], offsets[i,j,1], true);
                }
                else if ( layout[i,j] == 3) {
                    DrawCell(endZone, result, offsets[i,j,0], offsets[i,j,1]);
                }
            }
        }

        return result;

    }

    protected int[,] GenerateLayout() 
    {
        //Generate procedural layout for map tiles
        int[,] result = new int[4,4] {
            { 1, 1, 1, 1 }, 
            { 1, 1, 1, 1 }, 
            { 1, 1, 1, 1 }, 
            { 1, 1, 1, 1 } 
        };

        //Set spawnpoint
        int[] spawnpoint = new int[] {Random.Range(0,4), Random.Range(0,4)};

        result[spawnpoint[0], spawnpoint[1]] = 2;

        //Generate Rooms
        int numCells = Random.Range(3,14);
        int[] currentCell = spawnpoint;

        for (int i = numCells; i > 0; i--) 
        {
            currentCell = GetRandomAdjacentCell(currentCell, result);
            result[currentCell[0],currentCell[1]] = 0;
        }

        //Make last cell end zone
        result[currentCell[0],currentCell[1]] = 3;

        return result;
    }

    protected int[] GetRandomAdjacentCell(int[] cell, int[,] layout)
    {
        List<int[]> adjacentCells = GetAdjacentCells(cell, layout);

        if(adjacentCells.Count > 0) {
            int r = Random.Range(0, adjacentCells.Count);
            return adjacentCells[r];
        }
        else {
            return cell;
        }
    }

    protected List<int[]> GetAdjacentCells(int[] cell, int[,] layout)
    {
        List<int[]> result = new List<int[]> ();

        List<int[]> cardinals = new List<int[]> ();
        cardinals.Add( new int[] { cell[0] + 1, cell[1] });
        cardinals.Add( new int[] { cell[0] - 1, cell[1] });
        cardinals.Add( new int[] { cell[0], cell[1] + 1 });
        cardinals.Add( new int[] { cell[0], cell[1] - 1 });

        foreach(int[] i in cardinals){
            if(i[0] >= 0 && i[0] <= layout.GetUpperBound(0) && i[1] >= 0 && i[1] <= layout.GetUpperBound(1) && layout[i[0], i[1]] == 1){
                result.Add(i);
            }
        }

        return result;
    }

    protected int[] GetRandomCell(int[] currentCell, int[,] layout, int type){

        List<int> allCellValues = new List<int> ();

        foreach (int i in layout)
        {
            allCellValues.Add(i);
        }

        if(!allCellValues.Contains(type)) { return currentCell; }

        int[] result = new int[2];

        while (true) {
            int[] cell = {Random.Range(0,4), Random.Range(0,4)};
            if(layout[cell[0],cell[1]] == type)
            {
                result = cell;
                break;
            }
        }

        return result;
    }

    protected Texture2D RandomMap(Texture2D[] maps)
    {
        if(maps.Length < 1)
        {
            Debug.LogError("Cannot select map: no maps found.");
            return new Texture2D(16,16);
        }
        int index = Random.Range(0, maps.Length);
        return maps[index];
    }

}
