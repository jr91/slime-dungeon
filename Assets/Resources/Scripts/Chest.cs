using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{
    public bool isOpen = false;
    private Animator animator;

    public void Open() 
    {
        if(isOpen == true) {
            return;
        } else {
            animator = GetComponent<Animator> ();
            animator.SetTrigger("Open");
            isOpen = true;
        }

    }
}
