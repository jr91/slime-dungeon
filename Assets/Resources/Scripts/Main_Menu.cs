using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Main_Menu : MonoBehaviour
{
	public void PlayStart () {
		ResetPrefs();
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
	}

	public void Quit () {
		Application.Quit ();
	}

	void ResetPrefs () {
		//Reset all player prefs in case they weren't already wiped
		PlayerPrefs.DeleteKey("playerHealth");
		PlayerPrefs.DeleteKey("Level");
		PlayerPrefs.DeleteKey("Score");
	}
}
