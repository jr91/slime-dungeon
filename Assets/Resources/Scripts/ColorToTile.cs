﻿using UnityEngine;
using UnityEngine.Tilemaps;

[System.Serializable]
public class ColorToTile {

    public Color color;
    public RuleTile tile;
}

[System.Serializable]
public class ColorToPrefab
{
    public Color color;
    public GameObject prefab;
}
