using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;


public class Player : MonoBehaviour
{
    public int score = 0;
    public int health = 5;
    public int maxHealth = 5;
    public float moveSpeed = 5f;

    public Rigidbody2D rb;

    Vector2 movement;

    private float timeBtwAttack;
    public float startTimeBtwAttack;

    public Transform attackPos;
    public LayerMask enemiesMask;
    public float attackRange;
    public int damage;

    public GameObject scoreUI;
    public GameObject healthUI;
    public GameObject counterUI;
    public GameObject cameraReference;
    public Transform cursor;

    private float radius = 0.4f;
    private float cursorRadius;

    private int level;

    private bool running = false;
    private Animator animator; 

    //Audio variables
	public AudioClip[] audioClip;
	private AudioSource audioSrc;

    // Start is called before the first frame update
    void Start()
    {
        audioSrc = gameObject.GetComponent<AudioSource> ();
        animator = GetComponent<Animator> ();
        cursorRadius = radius + attackRange;
        health = PlayerPrefs.GetInt("playerHealth");
        level = PlayerPrefs.GetInt("Level");
        score = PlayerPrefs.GetInt("Score");
        if(health < 1 || health > maxHealth) {
            health = maxHealth;
        }
        if(level < 1) {
            level = 1;
        }
        if(score < 1) {
            score = 0;
        }
        UpdatePlayerUI();
    }

    // Update is called once per frame
    void Update()
    {
        //Movement inputs
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        //Update running variable
        if(movement.x != 0 || movement.y != 0) { running = true; } else { running = false;}

        //Flip sprite to follow cursor
        FaceCursor();

        //Rotate handheld item to face cursor
        DrawHandheldItem(transform);
        
        //Draw cursor UI
        DrawCursor(transform);

        //Attack timer
        if(timeBtwAttack <= 0){
            //attack
            if(Input.GetButtonDown("Fire1")){
                Attack();
            }
        } else {
            timeBtwAttack -= Time.deltaTime;
        }

    }

    void FixedUpdate()
    {
        UpdatePosition();
    }

    void OnTriggerEnter2D (Collider2D trig){
		//Trigger events
		if (trig.gameObject.tag == "Finish") {
            level += 1;
            PlayerPrefs.SetInt("Level", level);
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
		}
        if (trig.gameObject.tag == "Cell") {
            UpdateCurrentCell(trig.gameObject.transform);
        }
		if (trig.gameObject.tag == "Chest" && trig.gameObject.GetComponent<Chest> ().isOpen == false) {
			trig.gameObject.GetComponent<Chest> ().Open();
            score += 10;
            PlaySound(1);
            UpdatePlayerUI();
		}
        if(trig.gameObject.tag == "Potion") {
            Destroy(trig.gameObject);
            health = maxHealth;
            PlaySound(2);
            UpdatePlayerUI();
        }
    }

    void UpdatePosition() {

        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);

        //Running animation
        if(running == true) { animator.SetBool ("Running", true); } else if (running == false) { animator.SetBool ("Running", false); }
    
    }

    void UpdatePlayerUI() {
        healthUI.gameObject.GetComponent<HealthBar> ().SetHealth(health);
        counterUI.gameObject.GetComponent<Text> ().text = level.ToString();
        scoreUI.gameObject.GetComponent<Text> ().text = score.ToString();
    }

    void FaceCursor() {

        //Don't flip player sprite if game is paused
        if(Time.timeScale == 0){
            return;
        }
        //Flip player sprite along x axis to face relative position of cursor
        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        Vector3 transform = GetComponent<Transform> ().position;
        if(mousePosition.x < transform.x) { GetComponent<SpriteRenderer>().flipX = true; } else if (mousePosition.x > transform.x) { GetComponent<SpriteRenderer>().flipX = false; }
    }

    void DrawHandheldItem(Transform center) {
        if(Time.timeScale == 0){
            return;
        }

        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        float d = Mathf.Sqrt( Mathf.Pow(mousePosition.x - center.position.x, 2) + Mathf.Pow(mousePosition.y - center.position.y, 2));
        
        if ( d < radius ) {
            return;
        }

        //Calculate position of sword w/ fancy math to snap to a circle around the player and always face the mouse cursor
        float x = center.position.x + (radius / d) * (mousePosition.x - center.position.x);
        float y = center.position.y + (radius / d) * (mousePosition.y - center.position.y);

        attackPos.position = new Vector3 (x, y, center.position.z);
    }

    void DrawCursor(Transform center) {
        if(Time.timeScale == 0){
            return;
        }

        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        float d = Mathf.Sqrt( Mathf.Pow(mousePosition.x - center.position.x, 2) + Mathf.Pow(mousePosition.y - center.position.y, 2));
        
        if ( d < cursorRadius ) {
            cursor.position = Input.mousePosition;
            return;
        }

        float x = center.position.x + (cursorRadius / d) * (mousePosition.x - center.position.x);
        float y = center.position.y + (cursorRadius / d) * (mousePosition.y - center.position.y);

        Vector3 result = new Vector3 (x, y, center.position.z);

        cursor.position = Camera.main.WorldToScreenPoint(result);
    }

    void Attack() {

        Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, enemiesMask);
        for (int i = 0; i < enemiesToDamage.Length; i++) {
            enemiesToDamage[i].GetComponent<SlimeHealth> ().Hit(damage, attackPos.position, attackRange);
        }

        timeBtwAttack = startTimeBtwAttack;
    }

    public void Hit( int damage) {
        //animator.SetTrigger("Hit");
        health -= damage;

        UpdatePlayerUI();

        if( health < 1) {
            Die();
        } else {
            PlayerPrefs.SetInt("playerHealth", health);
            PlaySound(0);
        }
    }

    void Die () {
        PlayerPrefs.DeleteKey("playerHealth");
        PlayerPrefs.DeleteKey("Level");
        PlayerPrefs.DeleteKey("Score");
        SceneManager.LoadScene ("MainMenu");
    }

    public void PlaySound(int clip) {
		audioSrc.PlayOneShot(audioClip[clip]);
	}

    void UpdateCurrentCell(Transform cell) {
        //TODO: Make this less scuffed
        cameraReference.GetComponent<CameraSystem> ().currentCell = cell;
    }

    void OnDrawGizmosSelected(){
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }
}
