using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeBehavior : MonoBehaviour
{
    public int health = 2;
    public float moveSpeed = 1f;
    public float stoppingDistance = 1f;
    public float detectionRange = 5f;

    private Transform target;
    private bool detectedPlayer = false;
    private SpriteRenderer sprite;
    private Animator animator; 
    private Rigidbody2D rb;

    public bool spawnPrefabOnDeath = false;
    public GameObject prefab;

    private float timeBtwAttack;
    public float startTimeBtwAttack;

    //Audio variables
	public AudioClip[] audioClip;
	private AudioSource audioSrc;

    // Start is called before the first frame update
    void Start()
    {
        audioSrc = gameObject.GetComponent<AudioSource> ();
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        sprite = GetComponentsInChildren<SpriteRenderer>()[0];
        animator = GetComponentsInChildren<Animator>()[0];
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //Flip sprite to follow movement
        if(detectedPlayer == true) {
            if(transform.position.x > target.position.x) { sprite.flipX = true; } else if (transform.position.x < target.position.x) { sprite.flipX = false; }
        }
        else {
            LookForPlayer();
        }

        if(timeBtwAttack > 0) {
            timeBtwAttack -= Time.deltaTime;
        }
    }

    void FixedUpdate()
    {
        MoveTowardsPlayer();
    }

    void MoveTowardsPlayer()
    {
        if(detectedPlayer == true && Vector2.Distance(transform.position, target.position) > stoppingDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
        }
    }

    void LookForPlayer() {
        if (Vector2.Distance(transform.position, target.position) < detectionRange) {
            DetectPlayer();
        }
    }

    void DetectPlayer() {
        if(detectedPlayer == false) { detectedPlayer = true; }
    }

    void OnCollisionEnter2D (Collision2D col){
		//Collision events
		if (col.gameObject.tag == "Player") {
            if(timeBtwAttack <= 0){
			    Attack(col.gameObject);
            }
		}
    }

    void Attack (GameObject target) {
        target.GetComponent<Player> ().Hit(1);

        timeBtwAttack = startTimeBtwAttack;
    }

    public void Hit( int damage, Vector3 hitPos, float attackRadius, int kbBase = 200, int kbFactor = 500) {
        animator.SetTrigger("Hit");
        health -= damage;
        if( health < 1) {
            Die();
        } else {
            PlaySound(0);
            float kbForce = Mathf.Abs((1 - (Vector2.Distance(hitPos, transform.position) / attackRadius)) * kbFactor);
            rb.AddForce( -(hitPos - transform.position)* (kbBase + kbForce));
        }
    }

    void Die() {
        if(spawnPrefabOnDeath == true) {
            Instantiate(prefab, transform.position, Quaternion.identity);
        }
        Destroy(gameObject);
    }

    void PlaySound(int clip) {
		audioSrc.PlayOneShot(audioClip[clip]);
	}

}
